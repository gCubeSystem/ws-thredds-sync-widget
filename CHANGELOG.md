# Changelog for ws-thredds-sync-widget

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [v1.5.0] - 2021-07-20

#### Enhancements

[#21346] Moved to AccessTokenProvider for UMA tokens "context switches"
[#21576] Adding filtering for gateway to get scopes with THREDDS role for users
Moved to maven-portal-bom 3.6.3
Including new version of ws-thredds

## [v1.4.1-SNAPSHOT] - 2021-07-20

Moved to maven-portal-bom 3.6.3
Just to include new version of ws-thredds

## [v1.4.0] - 2021-05-10

#### Enhancements

[#21379] Moved to new ws-synchronized-module-library (based on w-thredds 1.x) and performed UMA tokens "context switches"
[#21444] Moved to maven-portal-bom 3.6.2

## [v1.3.0] - 2021-03-17

[#20847] Support the roles of THREDDS Admin and THREDDS Publisher 


## [v1.2.0] - 2020-07-21

[#19676] Migrated to git/jenkins


## [v1.1.0] - 2019-10-21

[#17348] Migrate ws-thredds-sync components to SHUB


## [v1.0.0] - 2018-03-16

First Release
