package org.gcube.portlets.widgets.wsthreddssync.client;


/**
 * The Class WsThreddsSyncConstants.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Mar 11, 2021
 */
public class WsThreddsSyncConstants {
	
	
	/**
	 * The Class RightsMissing.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
	 * 
	 * Mar 11, 2021
	 */
	public static class RightsMissing {
		
		/** The Constant REQUEST_IT_TO_THE_VRE_MANAGER_OR_THE_PORTAL_ADMINISTRATOR. */
		public static final String REQUEST_IT_TO_THE_VRE_MANAGER_OR_THE_PORTAL_ADMINISTRATOR = "Request it to the VRE manager or the portal administrator.";

		/**
		 * No run sync rights.
		 *
		 * @param scope the scope
		 * @return the string
		 */
		public static String noRunSyncRights(String scope) {
			
			StringBuilder builder = new StringBuilder("It seems you are not authorized to run a syncronization task towards THREDDS Catalogue");
			
			if(scope!=null)
				builder.append(" in the scope \""+scope+"\"");
			
			builder.append(". ");
			builder.append(REQUEST_IT_TO_THE_VRE_MANAGER_OR_THE_PORTAL_ADMINISTRATOR);
			
			
			return builder.toString();
		}
		
		/**
		 * No create config rights.
		 *
		 * @return the string
		 */
		public static String noCreateConfigRights() {
			
			StringBuilder builder = new StringBuilder("It seems you are not authorized to create a configuration towards THREDDS Catalogue.");
			builder.append(REQUEST_IT_TO_THE_VRE_MANAGER_OR_THE_PORTAL_ADMINISTRATOR);
			
			return builder.toString();
		}

		
	}

}
