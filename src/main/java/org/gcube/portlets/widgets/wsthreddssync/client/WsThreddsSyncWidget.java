package org.gcube.portlets.widgets.wsthreddssync.client;
//package org.gcube.portlets.widgets.wsthreddssync.client;
//
//import org.gcube.portlets.widgets.wsthreddssync.client.rpc.ThreddsWorkspaceSyncService;
//import org.gcube.portlets.widgets.wsthreddssync.client.rpc.ThreddsWorkspaceSyncServiceAsync;
//
//import com.google.gwt.core.client.EntryPoint;
//import com.google.gwt.core.client.GWT;
//
//// TODO: Auto-generated Javadoc
///**
// * Entry point classes define <code>onModuleLoad()</code>.
// *
// * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
// * Feb 14, 2018
// */
//public class WsThreddsSyncWidget implements EntryPoint {
//  /**
//   * The message displayed to the user when the server cannot be reached or
//   * returns an error.
//   */
//  private static final String SERVER_ERROR = "An error occurred while "
//      + "attempting to contact the server. Please check your network "
//      + "connection and try again.";
//
//  /** The ws thredds sync service. */
//  private final ThreddsWorkspaceSyncServiceAsync wsThreddsSyncService = GWT.create(ThreddsWorkspaceSyncService.class);
//
//  /**
//   * This is the entry point method.
//   */
//  public void onModuleLoad() {
//    
//  }
//}
