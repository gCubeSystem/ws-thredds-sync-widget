package org.gcube.portlets.widgets.wsthreddssync.server;

import java.net.MalformedURLException;
import java.net.URL;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * The Class GenericUtils.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Mar 3, 2021
 */
public class GenericUtils {
	
	public static Logger LOG = LoggerFactory.getLogger(GenericUtils.class);
	
	/**
	 * Gets the gateway client hostname.
	 *
	 * @param httpServletRequest the http servlet request
	 * @return the gateway client hostname
	 */
	public static String getGatewayClientHostname(HttpServletRequest httpServletRequest) {
		try {
			String clientURL = httpServletRequest.getRequestURL().toString();
			URL gURL = new URL(clientURL);
			StringBuilder theURL = new StringBuilder();
//			theURL.append(gURL.getProtocol());
//			theURL.append("://");
			theURL.append(gURL.getHost());
			return theURL.toString();
		} catch (MalformedURLException e) {
			LOG.warn("Failed to determine the gateway from the client url: "+httpServletRequest.getRequestURL());
			return null;
		}
	}
	


}
