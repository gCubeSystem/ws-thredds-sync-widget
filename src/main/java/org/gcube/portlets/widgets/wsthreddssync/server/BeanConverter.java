package org.gcube.portlets.widgets.wsthreddssync.server;

import org.gcube.portal.wssynclibrary.shared.thredds.ThSyncFolderDescriptor;
import org.gcube.portal.wssynclibrary.shared.thredds.ThSynchFolderConfiguration;
import org.gcube.portlets.widgets.wsthreddssync.shared.GcubeScope;
import org.gcube.portlets.widgets.wsthreddssync.shared.GcubeScopeType;
import org.gcube.portlets.widgets.wsthreddssync.shared.WsThreddsSynchFolderConfiguration;
import org.gcube.portlets.widgets.wsthreddssync.shared.WsThreddsSynchFolderDescriptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


// TODO: Auto-generated Javadoc
/**
 * The Class BeanConverter.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Aug 2, 2019
 */
public class BeanConverter {

	private static Logger logger = LoggerFactory.getLogger(BeanConverter.class);


	/**
	 * To ws thredds folder config.
	 *
	 * @param t the t
	 * @param theStatus the the status
	 * @return the ws thredds synch folder descriptor
	 */
	public static WsThreddsSynchFolderDescriptor toWsThreddsFolderConfig(ThSyncFolderDescriptor t) {

		if (t == null || t.getConfiguration()==null)
			return null;

		WsThreddsSynchFolderDescriptor ws = new WsThreddsSynchFolderDescriptor();
		ws.setServerFolderDescriptor(t);

		// FROM TARGET TOKEN TO SCOPE
		// t.getConfiguration().getTargetToken()
		
		String targetContext = t.getConfiguration().getTargetContext();

		if (targetContext != null) {
			try {
				GcubeScope selectedScope = new GcubeScope(toScopeTitle(targetContext), targetContext, toGcubeScope(targetContext));
				ws.setSelectedScope(selectedScope);
				logger.debug("Got target SCOPE: " + selectedScope + " from configuration");
			} catch (Exception e) {
				logger.error("I cannot read the scope for: "+targetContext);
			}
		}

		return ws;

	}

	/**
	 * To scope title.
	 *
	 * @param scope the scope
	 * @return the string
	 */
	public static String toScopeTitle(String scope) {

		if (scope == null || scope.isEmpty())
			return null;

		return scope.substring(scope.lastIndexOf("/") + 1, scope.length());

	}

	/**
	 * To gcube scope.
	 *
	 * @param scopeName the scope name
	 * @return the gcube scope type
	 */
	public static GcubeScopeType toGcubeScope(String scopeName) {

		if (scopeName == null)
			return null;
		
		String[] components=scopeName.split("/");
		
		if (components.length<2 || components.length>4) 
			throw new IllegalArgumentException("scope "+scopeName+" is malformed");
		
		if(components.length>3) {
			return GcubeScopeType.VRE;
		}
		else if (components.length>2) {
			return GcubeScopeType.VO;
		}
		else {
			return GcubeScopeType.ROOT;
		}
	}


	/**
	 * To th synch folder configuration.
	 *
	 * @param t the t
	 * @param rootFolderId the root folder id
	 * @param targetContext the target context
	 * @return the th synch folder configuration
	 */
	public static ThSynchFolderConfiguration toThSynchFolderConfiguration(WsThreddsSynchFolderConfiguration t,
			String rootFolderId, String targetContext) {

		if (t == null)
			return null;

		ThSynchFolderConfiguration ts = new ThSynchFolderConfiguration();
		ts.setFilter(t.getFilter());
		ts.setRemotePath(t.getRemotePath());
		ts.setTargetContext(targetContext);
		ts.setToCreateCatalogName(t.getCatalogName());
		ts.setRootFolderId(rootFolderId);
		return ts;

	};

}
