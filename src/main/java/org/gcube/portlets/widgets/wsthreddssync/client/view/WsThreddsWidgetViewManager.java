package org.gcube.portlets.widgets.wsthreddssync.client.view;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.gcube.portal.wssynclibrary.shared.thredds.Status;
import org.gcube.portal.wssynclibrary.shared.thredds.ThCatalogueBean;
import org.gcube.portal.wssynclibrary.shared.thredds.ThSyncStatus;
import org.gcube.portlets.widgets.wsthreddssync.client.SyncCompletedNotification.SyncCompletedNotificationListner;
import org.gcube.portlets.widgets.wsthreddssync.client.WsThreddsSyncConstants;
import org.gcube.portlets.widgets.wsthreddssync.client.WsThreddsWidget;
import org.gcube.portlets.widgets.wsthreddssync.client.event.PerformDoSyncEvent;
import org.gcube.portlets.widgets.wsthreddssync.client.event.ShowMonitorSyncStatusEvent;
import org.gcube.portlets.widgets.wsthreddssync.client.view.binder.AbstractViewDialogBox;
import org.gcube.portlets.widgets.wsthreddssync.client.view.binder.CreateThreddsConfigurationView;
import org.gcube.portlets.widgets.wsthreddssync.client.view.binder.MonitorFolderSyncStatusView;
import org.gcube.portlets.widgets.wsthreddssync.client.view.binder.ShowThreddsFolderInfoView;
import org.gcube.portlets.widgets.wsthreddssync.shared.GatewayRolesThredds;
import org.gcube.portlets.widgets.wsthreddssync.shared.GcubeScope;
import org.gcube.portlets.widgets.wsthreddssync.shared.WsFolder;
import org.gcube.portlets.widgets.wsthreddssync.shared.WsThreddsSynchFolderConfiguration;
import org.gcube.portlets.widgets.wsthreddssync.shared.WsThreddsSynchFolderDescriptor;

import com.github.gwtbootstrap.client.ui.Alert;
import com.github.gwtbootstrap.client.ui.Modal;
import com.github.gwtbootstrap.client.ui.constants.AlertType;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * The Class WsThreddsWidgetViewManager.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * Feb 19, 2018
 */
public class WsThreddsWidgetViewManager {

	public static final String CREATE_CONFIGURATION = "Create Configuration";
	public static final String THIS_FOLDER_IS_NOT_CONFIGURED_DO_YOU_WANT_CREATE_A_CONFIGURATION = "This Folder is not configured. Do you want create a configuration?";
	
	private Map<String, MonitorFolderSyncStatusView> mapMonitor = new HashMap<String, MonitorFolderSyncStatusView>();

	/**
	 * Instantiates a new ws thredds widget view manager.
	 */
	public WsThreddsWidgetViewManager() {
	}


	/**
	 * Cancel monitor.
	 *
	 * @param folder the folder
	 */
	public void cancelMonitor(WsFolder folder) {
		try {
			MonitorFolderSyncStatusView monitor = cancelScheduler(folder);
			if(monitor!=null) {
				GWT.log("Removed monitor for folder: "+folder.getFolderId());
				mapMonitor.remove(folder.getFolderId());
			}
		}catch (Exception e) {
			GWT.log("Error on removing map monitor for id: "+folder.getFolderId());
		}
	}


	/**
	 * Gets the monitor.
	 *
	 * @param folder the folder
	 * @return the monitor
	 */
	public MonitorFolderSyncStatusView getMonitor(WsFolder folder){

		if(folder==null)
			return null;

		return mapMonitor.get(folder.getFolderId());
	}


	/**
	 * Save monitor.
	 *
	 * @param folder the folder
	 * @param monitor the monitor
	 */
	private void saveMonitor(WsFolder folder, MonitorFolderSyncStatusView monitor) {
		GWT.log("Saving monitor for folder: "+folder.getFolderId());
		mapMonitor.put(folder.getFolderId(), monitor);
	}


	/**
	 * Cancel scheduler.
	 *
	 * @param folder the folder
	 * @return
	 */
	private MonitorFolderSyncStatusView cancelScheduler(final WsFolder folder) {

		MonitorFolderSyncStatusView monitor = getMonitor(folder);
		if(monitor.getSchedulerTime()!=null) {
			GWT.log("Cancelling scheduler time on folder: "+folder.getFolderId());
			monitor.getSchedulerTime().cancel();
		}

		return monitor;

	}

	/**
	 * Show monitor sync to folder.
	 *
	 * @param folder the folder
	 * @param syncEventsListeners
	 */
	public void showMonitorSyncToFolder(final WsFolder folder, final List<SyncCompletedNotificationListner> syncEventsListeners) {
		GWT.log("showMonitorSyncToFolder for folder: "+folder.getFolderId());

		final Modal box = new Modal(true);
		box.addStyleName("ws-thredds-modal-body");
		box.setTitle("Monitor transferring of: "+FormatUtil.getFolderTitle(folder.getFoderName(), 20));
		box.setWidth(800);
		box.hide(false);

		MonitorFolderSyncStatusView monitorView = getMonitor(folder);
		GWT.log("monitorView is: "+monitorView);

		final AbstractViewDialogBox panelView = new AbstractViewDialogBox() {

			@Override
			public void closeHandler() {
				//cancelScheduler(folder);
				box.hide();

			}

			@Override
			public void confirmHanlder(CONFIRM_VALUE confirm, Command command) {

				// TODO Auto-generated method stub

			}
		};

		if(monitorView==null) {

			monitorView = new MonitorFolderSyncStatusView() {

				@Override
				public void closetHandler() {
					//TODO CLIENT SIDE?
					//cancelMonitor(folder);
					panelView.closeHandler();
				}

				@Override
				public void setError(boolean visible, String error) {
					panelView.setError(visible, error);
				}
			};

		}

		if(monitorView.getSchedulerTime()==null) {

			//Removing old monitor
			//cancelMonitor(folder);
			//Creating new one
			final MonitorFolderSyncStatusView currentMonitor = monitorView;

			Timer schedulingTimer = new Timer() {

				@Override
				public void run() {

					WsThreddsWidget.wsThreddsSyncService.monitorSyncStatus(folder.getFolderId(), new AsyncCallback<ThSyncStatus>() {

						@Override
						public void onFailure(Throwable caught) {
							cancelMonitor(folder);
							Window.alert(caught.getMessage());

							//CALLING METHOD ON SYNC ERROR TO THE LISTENERS
							for (SyncCompletedNotificationListner listener : syncEventsListeners) {
								listener.onSyncError(folder);
							}
						}

						@Override
						public void onSuccess(ThSyncStatus status) {
							GWT.log("monitorSyncStatus: "+status);
							if(status==null) {
								GWT.log("The status is null server-side, cancelling polling");
								cancelMonitor(folder);
							}

							currentMonitor.updateStatusView(folder, status);

							if(status!=null && status.getProcessStatus()!=null) {

								if(status.getProcessStatus().getStatus().equals(Status.COMPLETED)) {
									GWT.log("Sync completed cancel the polling: "+status);
									cancelMonitor(folder);

									//CALLING METHOD ON SYNC COMPLETED TO THE LISTENERS
									for (SyncCompletedNotificationListner listener : syncEventsListeners) {
										listener.onSyncCompleted(folder);
									}
								}

							}
						}
					});
				}
			};

			schedulingTimer.scheduleRepeating(2000);
			currentMonitor.setScheduler(schedulingTimer);
			saveMonitor(folder, currentMonitor);

		}

		panelView.addViewAsWidget(monitorView);
		box.add(panelView);
		box.show();

	}


	/**
	 * Show create configuration folder.
	 *
	 * @param folder the folder
	 * @param conf the conf
	 */
	public void showCreateConfigurationFolder(final WsFolder folder, WsThreddsSynchFolderDescriptor conf){

		final Modal box = new Modal(true);
		box.setTitle("Create Thredds Sync Configuration for: "+FormatUtil.getFolderTitle(folder.getFoderName(), 20));
		//box.getElement().getStyle().setZIndex(10000);

		final AbstractViewDialogBox panelView = new AbstractViewDialogBox() {

			@Override
			public void closeHandler() {
				box.hide();

			}

			@Override
			public void confirmHanlder(CONFIRM_VALUE confirm, Command command) {

				if(confirm.equals(CONFIRM_VALUE.YES)){
					box.hide();
				}

				if(command!=null)
					command.execute();

			}
		};

		CreateThreddsConfigurationView createThreddsConfiguration = new CreateThreddsConfigurationView(folder.getFolderId()) {

			@Override
			public void submitHandler() {
				panelView.closeHandler();
				WsThreddsSynchFolderConfiguration conf = new WsThreddsSynchFolderConfiguration();
				conf.setFilter(null);

				ThCatalogueBean catalogueSelected = getSelectedCatalogue();
				String remotePath = catalogueSelected.getPath()!=null?catalogueSelected.getPath():"";
				remotePath = remotePath.isEmpty()?getFolderName():remotePath+"/"+getFolderName();
				conf.setRemotePath(remotePath);
				conf.setCatalogName(catalogueSelected.getName());
				conf.setSelectedScope(getSelectedScope());
				conf.setRootFolderId(folder.getFolderId());
				WsThreddsWidget.eventBus.fireEvent(new PerformDoSyncEvent(folder, conf));
			}

			@Override
			public void setError(boolean visible, String error) {
				panelView.setError(visible, error);

			}

			@Override
			public void setConfirm(boolean visible, String msg) {

				Command yes = new Command() {

					@Override
					public void execute() {

						submitHandler();
					}
				};

				Command no = new Command() {

					@Override
					public void execute() {

						panelView.setConfirm(false, "", null, null);
					}
				};

				panelView.setConfirm(visible, msg, yes, no);

			}
		};

		panelView.addViewAsWidget(createThreddsConfiguration);
		box.add(panelView);
		box.show();

	}


	/**
	 * Show thredds folder info.
	 *
	 * @param folder the folder
	 * @param folderDescriptor the folder descriptor
	 * @param mapScopeThreddsRoles the map scope with thredds roles
	 */
	public void showThreddsFolderInfo(final WsFolder folder, final WsThreddsSynchFolderDescriptor folderDescriptor, Map<String, GatewayRolesThredds> mapScopeThreddsRoles){
		GWT.log("ShowThreddsFolderInfo folder: "+folder);
		//GWT.log("WsThreddsSynchFolderDescriptor is: "+folderDescriptor);

		final Modal box = new Modal(true);
		box.getElement().addClassName("modal-sync-widget-config");
		box.setWidth(600);
		box.setTitle("Thredds Sync Information for: "+FormatUtil.getFolderTitle(folder.getFoderName(), 20));
	
		final AbstractViewDialogBox panelView = new AbstractViewDialogBox() {

			@Override
			public void closeHandler() {
				box.hide();
			}

			@Override
			public void confirmHanlder(CONFIRM_VALUE confirm, Command command) {

			}
		};

		final boolean isCreateConfiguration = folderDescriptor==null?true:false;
		
		//is the user a THREDDS_ADMIN (so DATA_MANAGER) in at least one scope?
		boolean isThreddsAdmin = false;
		
		if(mapScopeThreddsRoles!=null) {
			isThreddsAdmin = mapScopeThreddsRoles.containsValue(GatewayRolesThredds.DATA_MANAGER);
		}
		
		
		final ShowThreddsFolderInfoView folderInfo = new ShowThreddsFolderInfoView(folder.getFolderId(), isCreateConfiguration) {

			@Override
			public void submitHandler(SUBMIT_ACTION action) {
				panelView.closeHandler();

				if(action==null)
					return;

				switch (action) {

				case CREATE_UPDATE_CONFIGURATION:

					if(folderDescriptor==null) {
						GWT.log("Performing a create configuration");
						//PERFORM A CREATE CONFIGURATION (AT FIRST TIME), THE CONFIGURATION DOES NOT EXITS
						showCreateConfigurationFolder(folder, null);

					}
//					else {
//						GWT.log("Performing an updated configuration");
//						//PERFORM AN UPDATE CONFIGURATION. THE CONFIGURATION EXIST
//						//BeanConverter.toWsThreddsFolderConfig(t, vre);
//						showCreateConfigurationFolder(folder, result);
//					}
//

					break;

				case DO_SYNC:
					GWT.log("Performing a do sync using server folder configuration");
					//PERFORM A DO SYNC BUT NOT AT FIRST TIME
					WsThreddsWidget.eventBus.fireEvent(new PerformDoSyncEvent(folder, null));

					break;

				default:
					break;
				}

			}
			@Override
			public void setError(boolean visible, String error) {
				//panelView.setError(visible, error);
				panelView.setInfo(visible, error);
			}
		};

		folderInfo.updateViewToResult(folder, folderDescriptor);
		
		if(isCreateConfiguration) {
			folderInfo.getMainPanel().setVisible(false);
			
			//If the user is a THREDDS_ADMIN in at least one scope, he/she can create a configuration.
			if(isThreddsAdmin) {
				folderInfo.setError(true, THIS_FOLDER_IS_NOT_CONFIGURED_DO_YOU_WANT_CREATE_A_CONFIGURATION);
				folderInfo.getPager().getLeft().setText(CREATE_CONFIGURATION);
			}else {
				//Create Configuration button
				folderInfo.getPager().getLeft().setVisible(false);
				folderInfo.setError(true, WsThreddsSyncConstants.RightsMissing.noCreateConfigRights());
			}

		}else {
			//IT IS DO SYNC OR READ/VIEW THE CURRENT CONFIGURATION
			//MOREOVER, HE/SHE COULD UPDATE THE CONFIGURATION BUT IT IS NOT SUPPORTED SERVER-SIDE
			
			//hiding create/update configuration
			folderInfo.getPager().getLeft().setVisible(false);
			//hiding delete syncr
			folderInfo.showUnsyncButton(false);
			//doSync button
			folderInfo.getPager().getRight().setVisible(false);
			
			//Checking the user role in the target scope
			GcubeScope targetScope = folderDescriptor.getSelectedScope();
			
			boolean isThreddsPublisherInTheTargetScope = false;
			boolean isThreddsAdminInTheTargetScope = false;
			
			//the user has at least one role in one scope
			if(mapScopeThreddsRoles!=null) {
			
				GatewayRolesThredds rolesInTheTargetScope = mapScopeThreddsRoles.get(targetScope.getScopeName());
				GWT.log("In the target scope:  "+targetScope.getScopeName()+" the role of user is: "+rolesInTheTargetScope);
				//reading the role for the target scope
				if(rolesInTheTargetScope!=null) {
					isThreddsAdminInTheTargetScope = rolesInTheTargetScope.equals(GatewayRolesThredds.DATA_MANAGER);
					isThreddsPublisherInTheTargetScope = rolesInTheTargetScope.equals(GatewayRolesThredds.DATA_EDITOR);
				}
			}
			
			//the user is ADMIN or EDITOR in the target scope
			if(isThreddsPublisherInTheTargetScope || isThreddsAdminInTheTargetScope) {
				
				//he/she can run doSync
				folderInfo.getPager().getRight().setVisible(true);
				
				//showing unsync button if the user is an ADMIN
				if(isThreddsAdminInTheTargetScope) {
					folderInfo.showUnsyncButton(true);
				}

				if(folderDescriptor.getServerFolderDescriptor().isLocked()) {
					VerticalPanel v = new VerticalPanel();
					Alert alert = new Alert("Current Folder synchronization is locked by another proccess. Do you want see synchronization status?");
					alert.setClose(true);
					alert.setType(AlertType.INFO);

					Button butt = new Button("Show Status");
					butt.addClickHandler(new ClickHandler() {

						@Override
						public void onClick(ClickEvent event) {
							WsThreddsWidget.eventBus.fireEvent(new ShowMonitorSyncStatusEvent(folder));
						}
					});

					v.add(alert);
					v.add(butt);
					box.add(v);
				}
			}else {
				//DoSync button
				folderInfo.getPager().getRight().setVisible(false);
				folderInfo.setError(true, WsThreddsSyncConstants.RightsMissing.noRunSyncRights(targetScope.getScopeTitle()));
			}

		}
		

		panelView.addViewAsWidget(folderInfo);
		box.add(panelView);
		box.show();
		
	}

}
