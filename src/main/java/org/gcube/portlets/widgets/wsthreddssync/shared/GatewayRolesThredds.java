package org.gcube.portlets.widgets.wsthreddssync.shared;

 public enum GatewayRolesThredds {
	
	DATA_MANAGER("Data-Manager"),
	DATA_EDITOR("Data-Editor");
		
	private String name;
	
	private GatewayRolesThredds(String name) {
		this.name = name;
	}
	
	public String getRoleName() {
		return this.name;
	}

}
