/**
 *
 */
package org.gcube.portlets.widgets.wsthreddssync.server;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.gcube.common.portal.PortalContext;
import org.gcube.portlets.widgets.wsthreddssync.shared.GatewayRolesThredds;
import org.gcube.vomanagement.usermanagement.GroupManager;
import org.gcube.vomanagement.usermanagement.RoleManager;
import org.gcube.vomanagement.usermanagement.exception.GroupRetrievalFault;
import org.gcube.vomanagement.usermanagement.exception.UserManagementSystemException;
import org.gcube.vomanagement.usermanagement.exception.UserRetrievalFault;
import org.gcube.vomanagement.usermanagement.exception.VirtualGroupNotExistingException;
import org.gcube.vomanagement.usermanagement.impl.LiferayGroupManager;
import org.gcube.vomanagement.usermanagement.impl.LiferayRoleManager;
import org.gcube.vomanagement.usermanagement.model.GCubeGroup;
import org.gcube.vomanagement.usermanagement.model.GCubeRole;
//import org.gcube.portlets.user.workspace.server.util.WsUtil;
import org.gcube.vomanagement.usermanagement.model.GCubeUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.liferay.portal.service.UserLocalServiceUtil;

/**
 * The Class WsUtil.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it Nov 25, 2016
 */
public class WsUtil {

	/** The logger. */
	private static Logger logger = LoggerFactory.getLogger(WsUtil.class);

	/**
	 * Checks if is within portal.
	 *
	 * @return true if you're running into the portal, false if in development
	 */
	public static boolean isWithinPortal() {
		try {
			UserLocalServiceUtil.getService();
			return true;
		} catch (Exception ex) {
			logger.trace("Development Mode ON");
			return false;
		}
	}

	/**
	 * Checks if is session expired.
	 *
	 * @param httpServletRequest the http servlet request
	 * @return true, if is session expired
	 * @throws Exception the exception
	 */
	public static boolean isSessionExpired(HttpServletRequest httpServletRequest) throws Exception {
		logger.trace("workspace session validating...");
		return PortalContext.getConfiguration().getCurrentUser(httpServletRequest) == null;
	}

	/**
	 * Gets the list of Scopes (Root-VO, VOs and VREs) for user and the Thredds
	 * roles that user has in them.
	 *
	 * @param user the user
	 * @param gatewayHostname the gateway hostname
	 * @return the VREs and Thredds roles for a given user
	 */
	public static Map<String, GatewayRolesThredds> getScopesWithThreddsRolesForUser(GCubeUser user, String gatewayHostname) {
		logger.info("called getScopesThreddsRolesForUser user: " + user + ", in the gateway Contexts/VREs");

		GroupManager groupManager = new LiferayGroupManager();
		Map<String, GatewayRolesThredds> mapRoleByGroupSingleVre = new HashMap<String, GatewayRolesThredds>();
		try {
			
			long userId = user.getUserId();
			// list of Scopes filtered for gateway
			Set<GCubeGroup> filteredGroupsForGatw = groupManager.listGroupsByUserAndSite(userId, gatewayHostname);
			List<GCubeGroup> listOfGroups = new ArrayList<GCubeGroup>(filteredGroupsForGatw);
			logger.info("list of VREs for user "+user.getUsername()+" in the gateway " + gatewayHostname + " are: " + listOfGroups.size());
			if(logger.isDebugEnabled()) {
				for (GCubeGroup gCubeGroup : listOfGroups) {
					logger.info("the user "+user.getUsername()+" is registered in the VRE "+gCubeGroup.getGroupName());
				}
			}

			// adding also the ROOT-VO
			listOfGroups.add(groupManager.getRootVO());
			for (GCubeGroup gCubeGroup : listOfGroups) {
				GatewayRolesThredds threddsRole = getThreddsRoleFor(user, gCubeGroup);
				if (threddsRole != null) {
					String toFullScope = groupManager.getInfrastructureScope(gCubeGroup.getGroupId());
					mapRoleByGroupSingleVre.put(toFullScope, threddsRole);
				}
			}

			logger.info("For user: " + user + ", returning Map (VRE, ThreddsRoles)  " + mapRoleByGroupSingleVre);
			return mapRoleByGroupSingleVre;
		} catch (UserManagementSystemException | UserRetrievalFault | GroupRetrievalFault e) {
			logger.error("An error occurred during geThreddsVreRolesForUser: " + user, e);
			return null;
		} catch (VirtualGroupNotExistingException e) {
			logger.error("An error occurred during geThreddsVreRolesForUser: " + user, e);
			return null;
		}
	}

	/**
	 * Gets the (highest) thredds role for the user in the scope
	 *
	 * @param user  the user
	 * @param scope the vre
	 * @return the thredds role for
	 */
	public static GatewayRolesThredds getThreddsRoleFor(GCubeUser user, GCubeGroup scope) {
		if (user == null || scope == null) {
			logger.warn("called getThreddsRoleFor with invalid parameter user: " + user + ", in the scope: " + scope,
					", returning null");
			return null;
		}

		logger.info("called getThreddsRoleFor user: " + user.getUsername() + ", in the scope: " + scope.getGroupName());
		try {
			RoleManager roleManager = new LiferayRoleManager();
			List<GCubeRole> roles = roleManager.listRolesByUserAndGroup(user.getUserId(), scope.getGroupId());
			List<GatewayRolesThredds> threddsRoles = new ArrayList<GatewayRolesThredds>();
			for (GCubeRole gCubeRole : roles) {
				if (gCubeRole.getRoleName().equalsIgnoreCase(GatewayRolesThredds.DATA_MANAGER.getRoleName())) {
					threddsRoles.add(GatewayRolesThredds.DATA_MANAGER);
				}
				if (gCubeRole.getRoleName().equalsIgnoreCase(GatewayRolesThredds.DATA_EDITOR.getRoleName())) {
					threddsRoles.add(GatewayRolesThredds.DATA_EDITOR);
				}
			}

			logger.info("For user: " + user.getUsername() + " in the scope: " + scope.getGroupName()
					+ " read the role/s: " + threddsRoles);

			GatewayRolesThredds toReturn = null;
			if (threddsRoles.contains(GatewayRolesThredds.DATA_MANAGER))
				toReturn = GatewayRolesThredds.DATA_MANAGER;
			else if (threddsRoles.contains(GatewayRolesThredds.DATA_EDITOR))
				toReturn = GatewayRolesThredds.DATA_EDITOR;

			logger.info("returning highest role: " + toReturn);
			return toReturn;
		} catch (UserRetrievalFault | GroupRetrievalFault e) {
			logger.error("An error occurred during getVreRoleForUser: " + user, e);
			return null;
		}
	}

}
