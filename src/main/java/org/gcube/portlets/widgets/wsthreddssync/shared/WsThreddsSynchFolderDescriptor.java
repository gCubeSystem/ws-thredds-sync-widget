package org.gcube.portlets.widgets.wsthreddssync.shared;

import java.io.Serializable;

import org.gcube.portal.wssynclibrary.shared.thredds.ThSyncFolderDescriptor;

/**
 * The Class WsThreddsSynchFolderDescriptor.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         May 13, 2021
 */
public class WsThreddsSynchFolderDescriptor implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -5395986188613871699L;

	private GcubeScope selectedScope;

	private ThSyncFolderDescriptor serverFolderDescriptor;

	/**
	 * Instantiates a new ws thredds synch folder configuration.
	 */
	public WsThreddsSynchFolderDescriptor() {
	}

	/**
	 * Instantiates a new ws thredds synch folder descriptor.
	 *
	 * @param selectedScope          the selected scope
	 * @param serverFolderDescriptor the server folder descriptor
	 */
	public WsThreddsSynchFolderDescriptor(GcubeScope selectedScope, ThSyncFolderDescriptor serverFolderDescriptor) {
		super();
		this.selectedScope = selectedScope;
		this.serverFolderDescriptor = serverFolderDescriptor;
	}

	/**
	 * Gets the selected scope.
	 *
	 * @return the selectedScope
	 */
	public GcubeScope getSelectedScope() {

		return selectedScope;
	}

	/**
	 * Sets the selected scope.
	 *
	 * @param selectedVRE the new selected scope
	 */
	public void setSelectedScope(GcubeScope selectedVRE) {
		this.selectedScope = selectedVRE;
	}

	/**
	 * Gets the server folder descriptor.
	 *
	 * @return the server folder descriptor
	 */
	public ThSyncFolderDescriptor getServerFolderDescriptor() {
		return serverFolderDescriptor;
	}

	/**
	 * Sets the server folder descriptor.
	 *
	 * @param serverFolderDescriptor the new server folder descriptor
	 */
	public void setServerFolderDescriptor(ThSyncFolderDescriptor serverFolderDescriptor) {
		this.serverFolderDescriptor = serverFolderDescriptor;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("WsThreddsSynchFolderDescriptor [selectedScope=");
		builder.append(selectedScope);
		builder.append(", serverFolderDescriptor=");
		builder.append(serverFolderDescriptor);
		builder.append("]");
		return builder.toString();
	}

}
