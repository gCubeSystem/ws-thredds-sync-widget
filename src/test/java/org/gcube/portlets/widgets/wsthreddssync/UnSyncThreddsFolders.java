/**
 *
 */
package org.gcube.portlets.widgets.wsthreddssync;

import java.util.ArrayList;
import java.util.List;

import org.gcube.portlets.widgets.wsthreddssync.server.SyncronizeWithThredds;



/**
 * The Class UnSyncThreddsFolders.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * Mar 13, 2018
 */
public class UnSyncThreddsFolders {

	public static String DEFAULT_SCOPE = "/gcube"; //DEV
	public static String TEST_USER = "francesco.mangiacrapa";
	public static String TEST_USER_TOKEN = "TOKEN"; 

	private static List<String> lstUnSynchedItem = new ArrayList<String>();
	private static List<String> lstUnSynchedFailedItem = new ArrayList<String>();
	private static List<String> lstErrorItem = new ArrayList<String>();
	private static long totalAttempts = 0;
	
	static String scope = "/gcube/devsec";
	static String token = "TOKEN";


	public static SyncronizeWithThredds syncService = new SyncronizeWithThredds();



	/**
	 * The main method.
	 *
	 * @param args the arguments
	 * @throws WorkspaceFolderNotFoundException the workspace folder not found exception
	 * @throws InternalErrorException the internal error exception
	 * @throws HomeNotFoundException the home not found exception
	 */
	/*
	public static void main(String[] args) throws Exception {

		unsycFirstLevel(ws, ws.getRoot().getId(), false);

		System.out.println("UnSync completed");

		System.out.println("\nUnsync attempted: "+totalAttempts);

		System.out.println("\nTotal failed unsync: "+lstUnSynchedFailedItem.size());

		for (String string : args) {
			System.out.println("Failed unsync: "+string);
		}

		System.out.println("\nUnsynched "+lstUnSynchedItem.size() +" item/s");
		for (String string : lstUnSynchedItem) {
			System.out.println("Unsynched id: "+string);
		}

		System.out.println("\nErros on "+lstErrorItem.size() +" item/s");
		for (String string : lstErrorItem) {
			System.out.println("Error on id: "+string);
		}
	}



	public static void unsycFirstLevel(Workspace ws, String itemId, boolean depthUnsync) throws Exception{

		WorkspaceItem item;
		try {

			item = ws.getItem(itemId);

			if(item.isFolder()){

				List<? extends WorkspaceItem> children = ws.getChildren(item.getId());
				for (WorkspaceItem workspaceItem : children) {
					if(depthUnsync)
						unsycFirstLevel(ws, workspaceItem.getId(), depthUnsync);

					unsynFolder(workspaceItem);
				}
			}

		}catch (ItemNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			lstErrorItem.add(itemId);

		}catch (InternalErrorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			lstErrorItem.add(itemId);
		}

	}

	public static void unsynFolder(WorkspaceItem workspaceItem) {

		if(workspaceItem==null)
			return;

		try{
			ScopeProvider.instance.set(DEFAULT_SCOPE);
			SecurityTokenProvider.instance.set(TEST_USER_TOKEN);
			ThSyncFolderDescriptor synched = syncService.isItemSynched(workspaceItem.getId());
			System.out.println("Is the workspace item: "+workspaceItem.getId() + " synched? "+synched +", Is folder? "+workspaceItem.isFolder());

			if(synched != null){
				//Boolean unsynched = syncService.doUnSync(workspaceItem.getId(), false, DEFAULT_SCOPE, TEST_USER_TOKEN);
				Workspace ws = getWorkspace();
				cleanItem(workspaceItem, ws);
				totalAttempts++;
//				if(unsynched)
//					lstUnSynchedItem.add(workspaceItem.getId());
//				else
//					lstUnSynchedFailedItem.add(workspaceItem.getId());
			}

		}catch(Exception e){
			e.printStackTrace();
			lstErrorItem.add(workspaceItem.getId());
		}

	}

	static void cleanItem(WorkspaceItem item, Workspace ws) throws Exception {
	
//        Map<String, Object> props = ws.getMetadata(item.getId());
//        if(props.containsKey(Constants.WorkspaceProperties.TBS)) {
//            if(item.isFolder()) {
//                //props.addProperties(Constants.cleanedFolderPropertiesMap);
//                ws.updateMetadata(item.getId(), Constants.cleanedFolderPropertiesMap);
//                List<? extends WorkspaceItem> children = ws.getChildren(item.getId());
//                for(WorkspaceItem child : children)
//                    cleanItem(child, ws);
//            }else {
//            	ws.updateMetadata(item.getId(), Constants.cleanedFolderPropertiesMap);
//            }
//        }
    }
	*/


}
